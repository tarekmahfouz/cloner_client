<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PublicController extends InitController
{
    // Run Service from background
    // as we don't need to wait untill the process finish
    public function syncRequest(Request $request)
	{
        $code = 200;
        $message = 'done.';
        $data = [];

        try {
            //$url = 'http://127.0.0.1:8000/api/insert';
            $url = 'http://localhost/cloner_client/public/api/insert';
            
            $parts=parse_url($url);
            $fp = fsockopen($parts['host'],
                isset($parts['port'])?$parts['port']:80,
                $errno, $errstr, 2);
            $out = "GET ".$parts['path']." HTTP/1.1\r\n";
            $out.= "Host: ".$parts['host']."\r\n";
            $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
            $out.= "Connection: Close\r\n\r\n";
    
            fwrite($fp, $out);
            while (($line = fgets($fp)) !== false) {
                echo $line."<br/>";
            }
            fclose($fp);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function insert(Request $request)
    {
        // Set MAx Execution Time for local machine not Server.
        ini_set('max_execution_time', -1);

        $code = 200;
        $message = 'done.';
        $data = [];
        $url = 'http://localhost/cloner_server/public/api/retrieve';
        //$url = 'http://127.0.0.1:8888/api/retrieve';
        try {
            $notEmpty = true;
            $start_at = Carbon::now();
            while($notEmpty) {
                $user = $this->serviceObj->getOne('User');
                if($user) {
                    $response = Http::get($url, [
                        'limit' => 2000,
                        'phone' => $user->phone
                    ]);
                } else {
                   $response = Http::get($url, ['limit' => 2000]);
                }
                $body = $response->json();
                $data = (array)$body['data'];
                //return $data;
                if(count($data) == 0) {
                    $notEmpty = false;
                }
                $this->serviceObj->bulkInsert('User', $data);
                /* foreach($data as $item) {
                    $this->serviceObj->create('User', $item);
                } */
            }
            $end_at = Carbon::now();
            $data['minutes'] = $end_at->diffInMinutes($start_at);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
}
