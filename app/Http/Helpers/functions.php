<?php

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


function jsonResponse($code = 200, $message = '', $data = []) {
	return response()->json([
		'code' => $code,
		'message' => $message,
		'data' => $data ?? [],
	],$code);
}


function getCode($code) {
    return ($code >= 100 && $code <= 599) ? $code : 400;
}


function paginate($items, $perPage = 10, $page = null, $baseUrl = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

	$items = $items instanceof Collection ? $items : Collection::make($items);

    $items = new LengthAwarePaginator(array_values($items->forPage($page, $perPage)->toArray()), $items->count(), $perPage, $page, $options);

    if ($baseUrl) {
        $items->setPath($baseUrl);
    }

    return $items;
}