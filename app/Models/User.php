<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $guarded = [];
    protected $hidden = [ 'created_at', 'updataed_at', ];
    protected $dates = [ 'created_at', 'updataed_at','date' ];

    protected $casts = [
        'time' => 'datetime:H:i',
    ];

}
