<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function(){
    Route::get('sync', ['as' => 'sync', 'uses' => 'PublicController@syncRequest']);
    Route::get('insert', ['as' => 'insert', 'uses' => 'PublicController@insert']);
});
